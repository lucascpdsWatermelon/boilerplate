const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

var argv = require('minimist')(process.argv);

const envName = argv.envName || "default.js";
const env = require("./env/" + envName);

const outputPath = path.resolve(__dirname, 'build/deploy');

module.exports = {
  entry: {
    app: './src/index.js'
  },
  output: {
    path: outputPath,
    filename : "bundle.[hash].js"
  },
  plugins: [
    new webpack.DefinePlugin({
      ...env
    }),
    new HtmlWebpackPlugin({
      hash : true,
      path : outputPath,
      template : "./index.html"
    }),
    new CopyWebpackPlugin([
      {
        from: './static/**/*',
        to: outputPath
      }
    ])
  ],
  module: {
    rules : [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use : [
          {
            loader : "babel-loader"
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use : [
          {
            loader : "style-loader"
          },
          {
            loader : "css-loader"
          },
          {
            loader : "sass-loader"
          }
        ]
      },
    ]
  }
};
