import React from 'react'
import ReactDOM from 'react-dom'

import {createStore} from 'redux'
import reducers from './redux/reducer'

import {Provider} from 'react-redux'

import Routes from './routes'

const store = createStore(reducers);

ReactDOM.render(
  (
    <Provider store={store}>
      <Routes/>
    </Provider>
  ),
  document.getElementById('app')
);
