import React, {Component} from 'react'

require('../style/page/basePage.scss')
class BasePage extends Component {

  render () {
    return (
      <div className="base-page">
        {this.props.children}
      </div>
    )
  }
}

export default BasePage;
