import React, {Component} from 'react'
import BasePage from './BasePage'

import {MockComponent} from 'react-watermelon'

require('../style/page/homePage.scss')
class HomePage extends Component {
  render () {
    return (
      <BasePage>
        <div className="home-page">
          HOME PAGE
          <MockComponent/>
        </div>
      </BasePage>
    )
  }
}

export default HomePage;
