import React from 'react'
import {Switch, BrowserRouter, Route} from 'react-router-dom'

import HomePage from './page/HomePage'

export default () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={HomePage}/>
      </Switch>
    </BrowserRouter>
  )

}
